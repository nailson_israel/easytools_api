const express = require('express');

const DashRoutes = express.Router();

DashRoutes.get('/highlights/', function(req, res) {
	res.json({
		text: 'highlights',
	})
});

DashRoutes.get('/news', function(req, res) {
	res.json({
		text: 'news',
	})
});

DashRoutes.get('/kits', function(req, res) {
	res.json({
		text: 'kits tools',
	})
});

module.exports = DashRoutes;