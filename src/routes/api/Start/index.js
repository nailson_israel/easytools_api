const express = require('express');

const StartRoutes = express.Router();

StartRoutes.get('/', function(req, res) {
	res.json({
		title: 'Eastytools Api',
		description: 'This is the Easytools Api. Easytools is the first tool rental platform of the Brazil, developed by Codens.',
		documentation: '',
		links: {
			link1: '',
			link2: '',
		}
	});
});

StartRoutes.get('/home', function(req, res) {
	res.json({
		text: 'home',
	})
});
module.exports = StartRoutes;