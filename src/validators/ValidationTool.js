const { check } = require('express-validator');

exports.validationStore = () => {
  return [
    check('title')
    .isLength({ min: 5 })
    .withMessage('Ops! O título do anúncio deve conter pelo menos 1 caracter'),
	]
}
