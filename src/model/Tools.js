const mongoose = require('mongoose');

const ToolsSchema = new mongoose.Schema({
    title: String,
    description: String,
    specification: {
      category: String,
      type_spec: String,
      accessory: String,
      brand: String,
      follow: String,
      use_indication: String,
      last_maintenance: Date,
    },
    setting: {
    },
    manual: String,
});

module.exports = mongoose.model('Tools', ToolsSchema);