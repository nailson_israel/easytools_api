const express = require('express');
const bcrypt = require('bcrypt');

const UserRoutes = express.Router();

UserRoutes.post('/create', function(req, res) {
	let hash = bcrypt.hashSync(req.body.password, 10);
	
	const { name } = req.body
	const { lastname } = req.body
	const { email } = req.body
	const { flag_mail } = req.body
	const { birth_date } = req.body
	const  password  = hash

	res.json({
		text: req.body,
	})
});

module.exports = UserRoutes;