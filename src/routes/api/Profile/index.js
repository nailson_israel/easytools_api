const express = require('express');

const ProfileRoutes = express.Router();

ProfileRoutes.get('/', function(req, res) {
	res.json({
		text: 'profile of the user',
	})
});

ProfileRoutes.post('/documents', function(req, res) {
	res.json({
		text: 'post documents',
	})
});

ProfileRoutes.put('/:id/', function(req, res) {
	const { id } = req.params
	res.json({
		text: 'updating',
	})
});

module.exports = ProfileRoutes;