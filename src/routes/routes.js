const express = require('express');

const routes = express.Router();

routes.get('/', function(req, res) {
	res.redirect('/start');
});

routes.use('/start', require('./api/Start/index'));
routes.use('/user/', require('./api/User/index'));
routes.use('/profile/', require('./api/Profile/index'));
routes.use('/dash/', require('./api/Dashboard/index'));
routes.use('/tools/', require('./api/Tool/index'));

routes.all('*', (req, res) => {
	res.status(404).json({msg: 'not found'});
});

module.exports = routes;