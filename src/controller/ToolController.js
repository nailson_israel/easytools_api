const { validationResult } = require('express-validator');
const Tool = require('../model/Tools');

module.exports = {
  async store(req, res) { 
    
    const { title } = req.body;
    const { description } = req.body;
    const { specification } = req.body;
    const { category } = specification;
    const { type } = specification;
    const {accessory } = specification;
    const { brand } = specification;
    const { follow } = specification;
    const { use_indication } = specification;
    const { last_maintenance } = specification;
    const { setting } = req.body;
    const { manual } = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

		const tool = await Tool.create({
      title: title,
      description: description,
      specification:{
        category: category,
        type_spec: type,
        accessory: accessory,
        brand: brand,
        follow: follow,
        use_indication: use_indication,
        last_maintenance: last_maintenance
      },
      setting,
      manual: manual,
    });

    return res.json(tool)
  }  
}
