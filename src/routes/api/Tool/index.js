const express = require('express');

const ToolController = require('../../../controller/ToolController');
const validationTool = require('../../../validators/ValidationTool');

const ToolsRoutes = express.Router();

ToolsRoutes.get('/', function(req, res) {
	res.json({
		text: 'get tools with parameters, localization, gps, mph',
	})
});

ToolsRoutes.post('/add/',
	validationTool.validationStore(),
	ToolController.store
);

ToolsRoutes.put('/:id/', (req, res) => {
	res.json({
		text: 'update tools',
	})
});

module.exports = ToolsRoutes;